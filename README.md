# Outreach workshops

This is a git repo to store and share materials associated with outreach activities.

The aim is to 

 * collect existing workshops in one place
 * create and populate a LaTeX template for workshop handouts
 * create and populate a "teachers' guide" type document where appropriate

In this way we can encourage reuse of existing materials.

The repo is structured with three base folders:

 * *Meta* to contain templates and files about the project
 * *Standalone* to contain structured materials concerning workshops that can be completed in a day
 * *Extended* to contain structured materials for workshops that fit well in a longer program of work for example an after school club, a class, or a series of school visits
 * *Original* for original materials which haven't been reworked or reformatted yet

# Standalone workshops

# Extended workshops and activities
