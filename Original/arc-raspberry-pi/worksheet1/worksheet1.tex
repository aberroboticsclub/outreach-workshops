% Template for documenting your Arduino projects
% Author:   Luis José Salazar-Serrano
%           totesalaz@gmail.com / luis-jose.salazar@icfo.es
%           http://opensourcelab.salazarserrano.com

%%% Template based in the template created by Karol Kozioł (mail@karol-koziol.net)

\documentclass[a4paper,11pt]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[UKenglish]{babel}% http://ctan.org/pkg/babel
\usepackage[UKenglish]{isodate}% http://ctan.org/pkg/isodate

\renewcommand\familydefault{\sfdefault}
\usepackage{tgheros}
\usepackage[defaultmono]{droidmono}

\usepackage{amsmath,amssymb,amsthm,textcomp}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{lastpage}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{courier}
\usepackage[hidelinks]{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\usepackage{geometry}
\geometry{total={210mm,297mm},
left=25mm,right=25mm,%
bindingoffset=0mm, top=20mm,bottom=20mm}


\linespread{1.3}

%\newcommand{\linia}{\rule{\linewidth}{0.5pt}}

% my own titles
%\makeatletter
%\renewcommand{\maketitle}{
%\begin{center}
%\vspace{2ex}
%{\huge \textsc{\@title}}
%\vspace{1ex}
%{\huge \textsc{\@subtitle}}
%\vspace{1ex}
%\\
%\linia\\
%\@author \hfill \@date
%\vspace{4ex}
%\end{center}
%}
%\makeatother
%%%

% custom footers and headers
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Version 1.1 | Draft}
\chead{Raspberry Pi Project - Worksheet 1}
\rhead{\today}
%\lfoot{}
\cfoot{}
\fancyfoot[R]{Page \thepage\ of {\hypersetup{linkcolor=black}\pageref{LastPage}}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%

% code listing settings
\usepackage{listings}

\lstset{%
  language = Octave,
  lineskip={-1.5pt},
  backgroundcolor=\color{white},   
  basicstyle=\footnotesize\ttfamily,       
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                   
  commentstyle=\color{gray},    
  deletekeywords={...},           
  escapeinside={\%*}{*)},          
  extendedchars=true,              
  frame=single,                    
  keepspaces=true,                 
  keywordstyle=\color{orange},       
  morekeywords={*,...},            
  numbers=left,                    
  numbersep=5pt,                   
  numberstyle=\footnotesize\color{gray}, 
  rulecolor=\color{black},         
  rulesepcolor=\color{blue},
  showspaces=false,                
  showstringspaces=false,          
  showtabs=false,                  
  stepnumber=1,                    
  stringstyle=\color{orange},    
  tabsize=2,                       
  title=\lstname,
  emphstyle=\bfseries\color{blue}%  style for emph={} 
} 

%% language specific settings:
\lstdefinestyle{Arduino}{%
    language = Octave,
    keywords={void, int boolean},%                 define keywords
    morecomment=[l]{//},%             treat // as comments
    morecomment=[s]{/*}{*/},%         define /* ... */ comments
    emph={HIGH, INPUT, OUTPUT, LOW}%        keywords to emphasize
}


%%%----------%%%----------%%%----------%%%----------%%%

\begin{document}

\title{Aberystwyth Robotics Club\\Raspberry Pi Project\\Worksheet 1\\Introduction and Setup}
%\author{}
\date{\parbox{\linewidth}{\centering%
  \today\endgraf\bigskip
  Version 1.1 \hspace*{3cm} Draft\endgraf\medskip
  }}



\maketitle
\begin{center}
\includegraphics[width=5cm]{img/logo.png}
\end{center}
\newpage


\section{Project Introduction}
The aim of these next few weeks is to introduce you to the Raspberry Pi!\newline

\noindent
The reason for doing this is because we have, so far, only covered Arduinos. When you come back in October; for the next term of the robotics club, you will be working on your own personal projects. For this you may need more than just an Arduino to control the robot by.\newline

This project will teach you the following:\newline

\begin{enumerate}[nolistsep]
  \item Setup a Raspberry Pi
  \item Personalise the Raspberry Pi
  \item Learn some basic UNIX commands to move around the file structures and find some statistics (new language)
  \item Connect up some electronics to the Pi (LEDs in our case)
  \item Installing libraries and interacting with hardware manually
  \item Installing Apache and PHP5 to create a web server
  \item Create static web pages using HTML (new language)
  \item Create dynamic web pages using PHP (new language)
  \item Creating a control interface for LEDs
  \item Hopefully it will all work!
\end{enumerate}
\newpage

\section{Raspberry Pi Start-up}
The Raspberry Pi is an amazing £32 mini-computer. It allows you to do everything you could do with a regular Linux computer (Connecting to the internet, watching videos of cats getting scared by cucumbers, launching applications, ...) but also to interact with the world surrounding it, just like an Arduino you have been using up to this point.

\begin{center}
\includegraphics[width=10cm]{img/pi.jpg}
\end{center}
It was created by the \href{https://www.raspberrypi.org/}{Raspberry Pi Foundation} to get computers into the hands of kids around the world, and to teach them to code. Since its initial launch, it’s become the most popular computer for DIYers who want to make their own electronics projects, from \href{http://lifehacker.com/5929913/build-a-xbmc-media-center-with-a-35-raspberry-pi}{media centers} to \href{http://lifehacker.com/how-to-turn-your-raspberry-pi-into-a-retro-game-console-498561192}{video game systems}. The Raspberry Pi also has it’s own official operating system, \href{https://www.raspbian.org/}{Raspbian}.



\section{What You’ll Need}
\begin{itemize}[nolistsep]
    \item \textbf{A Raspberry Pi:}
    \item \textbf{A HDMI monitor:} You’ll need to connect your Raspberry Pi to a display, which means you’ll need an HDMI-enabled screen
    \item \textbf{A USB keyboard and mouse:} In order to control your Pi, you’ll need a keyboard and mouse. At this point, pretty much any USB keyboard and mouse will work.
    \item \textbf{A MicroSD card and card reader:} Instead of a hard drive, you install the Raspberry Pi’s operating system on a MicroSD card. You’ll want at least an 8GB card for this.
    \item \textbf{A power supply:} The Raspberry Pi is powered by a micro USB, much like the one you’ve likely used for your phone. Since the Pi 3 has four USB ports, it’s best to use a good power supply that can provide at least 2.5A of power.
    \item \textbf{Download and Burn the OS: }This part has already been done for you.
\end{itemize}

\section{Set Up Your Raspberry Pi}
\begin{center}
\includegraphics[width=10cm]{img/pi-plugged.jpg}
\end{center}
Finally, once everything is connected, you will need to install the NOOBS software on the SD card before you power it on.


\section{How to install NOOBS on an SD card}
\textbf{\textit{The following has already been done for you as it does take about 30 minutes for the software to install, however it is important you read it as you may want to do it for your projects next term.}}
Once you've downloaded the NOOBS zip file, you'll need to copy the contents to a formatted SD card on your computer.\newline
NOOBS can be found on Simba Share - raspberry-pi\newline
To set up a blank SD card with NOOBS:
\begin{itemize}[nolistsep]
  \item Format an SD card which is 8GB or larger as FAT.
  \item Copy and paste the entire contents of NOOBS\_V2\_1\_0 on to the SD card (don't copy the folder)
  \item On first boot, the ``RECOVERY'' FAT partition will be automatically resized to a minimum, and a list of OSes that are available to install will be displayed.
  \item  We want to install Raspbian for our Project
  \item Then click install and wait 5 minutes for Raspbian to install to the SD card
\end{itemize}
Once you have finished this, move along to worksheet 2, an introduction to the Raspbian OS and UNIX


%\section*{Document History}
%\begin{center}
%\begin{tabular}{ |p{2cm}|p{5cm}|p{5cm}|p{4cm}| } 
%\hline
%Version & Changed & Changed By & Date Changed \\
%\hline
%V1.0 & Initial Version on moodle & Tomos & 8th February 2017\\ 
%\hline
%V1.1 & Updated layout for printing & Tomos & 17th November 2017\\
%\hline
%\end{tabular}
%\end{center}

\end{document}
