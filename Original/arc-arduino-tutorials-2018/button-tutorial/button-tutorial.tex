\documentclass[a4paper,11pt]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[UKenglish]{babel}% http://ctan.org/pkg/babel
\usepackage[UKenglish]{isodate}% http://ctan.org/pkg/isodate

\renewcommand\familydefault{\sfdefault}
\usepackage{tgheros}
\usepackage[defaultmono]{droidmono}

\usepackage{amsmath,amssymb,amsthm,textcomp}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{lastpage}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{courier}

\usepackage{geometry}
\geometry{total={210mm,297mm},
left=25mm,right=25mm,%
bindingoffset=0mm, top=20mm,bottom=20mm}


\linespread{1.3}

%\newcommand{\linia}{\rule{\linewidth}{0.5pt}}

% my own titles
%\makeatletter
%\renewcommand{\maketitle}{
%\begin{center}
%\vspace{2ex}
%{\huge \textsc{\@title}}
%\vspace{1ex}
%{\huge \textsc{\@subtitle}}
%\vspace{1ex}
%\\
%\linia\\
%\@author \hfill \@date
%\vspace{4ex}
%\end{center}
%}
%\makeatother
%%%

% custom footers and headers
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Version 1.0 | Release}
\chead{Arduino Tutorials - Button Tutorial}
\rhead{\today}
%\lfoot{Tomos Fearn (tomos@aberrobotics.club)}
\cfoot{}
\fancyfoot[R]{Page \thepage\ of \pageref{LastPage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%

% code listing settings
\usepackage{listings}

\lstset{%
  language = Octave,
  lineskip={-1.5pt},
  backgroundcolor=\color{white},   
  basicstyle=\footnotesize\ttfamily,       
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                   
  commentstyle=\color{gray},    
  deletekeywords={...},           
  escapeinside={\%*}{*)},          
  extendedchars=true,              
  frame=single,                    
  keepspaces=true,                 
  keywordstyle=\color{orange},       
  morekeywords={*,...},            
  numbers=left,                    
  numbersep=5pt,                   
  numberstyle=\footnotesize\color{gray}, 
  rulecolor=\color{black},         
  rulesepcolor=\color{blue},
  showspaces=false,                
  showstringspaces=false,          
  showtabs=false,                  
  stepnumber=1,                    
  stringstyle=\color{orange},    
  tabsize=2,                       
  title=\lstname,
  emphstyle=\bfseries\color{blue}%  style for emph={} 
} 

%% language specific settings:
\lstdefinestyle{Arduino}{%
    language = Octave,
    keywords={void, int boolean},%                 define keywords
    morecomment=[l]{//},%             treat // as comments
    morecomment=[s]{/*}{*/},%         define /* ... */ comments
    emph={HIGH, INPUT, OUTPUT, LOW}%        keywords to emphasize
}


%%%----------%%%----------%%%----------%%%----------%%%

\begin{document}

\title{Aberystwyth Robotics Club\\Arduino Tutorials\\Button Tutorial}
%\author{Tomos Fearn (tomos@aberrobotics.club)}
\date{\parbox{\linewidth}{\centering%
  \today\endgraf\bigskip
  Version 1.0 \hspace*{3cm} Release\endgraf\medskip
  %Dept.\ of Physics \endgraf
  %ABC College
  }}



\maketitle
\begin{center}
\includegraphics[width=5cm]{img/logo.png}
\end{center}
\newpage

\begin{center}
\section*{Abstract}
This example shows you how to monitor the state of a switch by establishing serial communication between your Arduino or Genuino and your computer over USB.
\end{center}



\section{Hardware Required}
\begin{itemize}[nolistsep]
  \item Arduino or Genuino Board
  \item A momentary switch, button, or toggle switch
  \item 10k ohm resistor
  \item hook-up wires
  \item breadboard
\end{itemize}

\section{Circuit}
Connect three wires to the board. The first two, red and black, connect to the two long vertical rows on the side of the breadboard to provide access to the 5 volt supply and ground. The third wire goes from digital pin 2 to one leg of the pushbutton. That same leg of the button connects through a pull-down resistor (here 10k ohm) to ground. The other leg of the button connects to the 5 volt supply.\newline
When the push button is open (unpressed) there is no connection between the two legs of the pushbutton, so the pin is connected to ground (through the pull-down resistor) and we read a LOW. When the button is closed (pressed), it makes a connection between its two legs, connecting the pin to 5 volts, so that we read a HIGH. (This may be represented as a 1 and a 0 in Arduino).\newline
You can also wire this circuit the opposite way, with a pullup resistor keeping the input HIGH, and going LOW when the button is pressed. If so, the behaviour of the sketch will be reversed, with the LED normally on and turning off when you press the button.\newline
If you disconnect the digital i/o pin from everything, the LED may blink erratically. This is because the input is ``floating'' - that is, it doesn't have a solid connection to voltage or ground, and it will randomly return either HIGH or LOW. That's why you need a pull-down resistor in the circuit.


\begin{figure}[h]
\centering
\includegraphics[width=9cm]{img/circuit.png}\includegraphics[height=4cm]{img/schematic.png}
\caption{Connecting a Button to an Arduino via a bread-board}
\end{figure}
\newpage
\section{Code}
After you build the circuit plug your Arduino or Genuino board into your computer, start the Arduino Software (IDE) and enter the code below\footnote{https://www.arduino.cc/en/tutorial/button}. In the program below, the very first thing that you do will in the setup function is to begin serial communications, at 9600 bits of data per second, between your board and your computer with the line: 

\begin{lstlisting}[label={list:sixth}, style=Arduino, caption=Start USB connection at 9600 baud]
Serial.begin(9600);
\end{lstlisting}

\noindent
Next, initialize digital pin 2, the pin that will read the output from your button, as an input: 

\begin{lstlisting}[label={list:seventh}, style=Arduino, caption=Set pin 2 as an Input]
pinMode(2,INPUT);
\end{lstlisting}

\noindent
Now that your setup has been completed, move into the main loop of your code. When your button is pressed, 5 volts will freely flow through your circuit, and when it is not pressed, the input pin will be connected to ground through the 10k ohm resistor. This is a digital input, meaning that the switch can only be in either an on state (seen by your Arduino as a ``1'', or HIGH) or an off state (seen by your Arduino as a ``0'', or LOW), with nothing in between.\newline
The first thing you need to do in the main loop of your program is to establish a variable to hold the information coming in from your switch. Since the information coming in from the switch will be either a ``1'' or a ``0'', you can use an int datatype. Call this variable sensorValue, and set it to equal whatever is being read on digital pin 2. You can accomplish all this with just one line of code: 

\begin{lstlisting}[label={list:eighth}, style=Arduino, caption=save values from pin 2 to variable sensorValue]
int sensorValue = digitalRead(2);
\end{lstlisting}

\noindent
Once the bord has read the input, make it print this information back to the computer as a decimal value. You can do this with the command Serial.println() in our last line of code: 

\begin{lstlisting}[label={list:first}, style=Arduino, caption=print off sensorValue to serial monitor]
Serial.println(sensorValue);
\end{lstlisting}

\noindent
Now, when you open your Serial Monitor in the Arduino Software (IDE), you will see a stream of ``0''s if your switch is open, or ``1''s if your switch is closed. 

\begin{lstlisting}[label={list:ninth}, style=Arduino, caption=Example Code]
int pushButton = 2; // digital pin 2 has a pushbutton attached to it.

void setup() {
  Serial.begin(9600);//initialize serial communication at 9600 bits per second:
  pinMode(pushButton, INPUT); // make the pushbutton's pin an input:
}

void loop() { // the loop function runs over and over again forever
  int buttonState = digitalRead(pushButton); // read the input pin:
  Serial.println(buttonState); // print out the state of the button:
  delay(1); // delay in between reads for stability
}
\end{lstlisting}


\end{document}
