\documentclass[a4paper,11pt]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[UKenglish]{babel}% http://ctan.org/pkg/babel
\usepackage[UKenglish]{isodate}% http://ctan.org/pkg/isodate

\renewcommand\familydefault{\sfdefault}
\usepackage{tgheros}
\usepackage[defaultmono]{droidmono}

\usepackage{amsmath,amssymb,amsthm,textcomp}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{lastpage}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{courier}

\usepackage{geometry}
\geometry{total={210mm,297mm},
left=25mm,right=25mm,%
bindingoffset=0mm, top=20mm,bottom=20mm}


\linespread{1.3}

%\newcommand{\linia}{\rule{\linewidth}{0.5pt}}

% my own titles
%\makeatletter
%\renewcommand{\maketitle}{
%\begin{center}
%\vspace{2ex}
%{\huge \textsc{\@title}}
%\vspace{1ex}
%{\huge \textsc{\@subtitle}}
%\vspace{1ex}
%\\
%\linia\\
%\@author \hfill \@date
%\vspace{4ex}
%\end{center}
%}
%\makeatother
%%%

% custom footers and headers
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Version 1.1 | Release}
\chead{Arduino Tutorials - LDR Tutorial}
\rhead{\today}
%\lfoot{Tomos Fearn (tomos@aberrobotics.club)}
\cfoot{}
\fancyfoot[R]{Page \thepage\ of \pageref{LastPage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%

% code listing settings
\usepackage{listings}

\lstset{%
  language = Octave,
  lineskip={-1.5pt},
  backgroundcolor=\color{white},   
  basicstyle=\footnotesize\ttfamily,       
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                   
  commentstyle=\color{gray},    
  deletekeywords={...},           
  escapeinside={\%*}{*)},          
  extendedchars=true,              
  frame=single,                    
  keepspaces=true,                 
  keywordstyle=\color{orange},       
  morekeywords={*,...},            
  numbers=left,                    
  numbersep=5pt,                   
  numberstyle=\footnotesize\color{gray}, 
  rulecolor=\color{black},         
  rulesepcolor=\color{blue},
  showspaces=false,                
  showstringspaces=false,          
  showtabs=false,                  
  stepnumber=1,                    
  stringstyle=\color{orange},    
  tabsize=2,                       
  title=\lstname,
  emphstyle=\bfseries\color{blue}%  style for emph={} 
} 

%% language specific settings:
\lstdefinestyle{Arduino}{%
    language = Octave,
    keywords={void, int boolean},%                 define keywords
    morecomment=[l]{//},%             treat // as comments
    morecomment=[s]{/*}{*/},%         define /* ... */ comments
    emph={HIGH, INPUT, OUTPUT, LOW}%        keywords to emphasize
}


%%%----------%%%----------%%%----------%%%----------%%%

\begin{document}

\title{Aberystwyth Robotics Club\\Arduino Tutorials\\Light Dependant Resistor Tutorial}
%\author{Tomos Fearn (tomos@aberrobotics.club)}
\date{\parbox{\linewidth}{\centering%
  \today\endgraf\bigskip
  Version 1.1 \hspace*{3cm} Release\endgraf\medskip
  %Dept.\ of Physics \endgraf
  %ABC College
  }}



\maketitle
\begin{center}
\includegraphics[width=5cm]{img/logo.png}
\end{center}
\newpage

\begin{center}
\abstract{
\noindent
In order to detect the intensity of light or darkness, we use a sensor called an LDR (Light Dependent Resistor). The LDR is a special type of resistor which allows higher voltages to pass through it (low resistance) whenever there is a high intensity of light, and passes a low voltage (high resistance) whenever it is dark.}
\end{center}



\section{Hardware Required}
\begin{itemize}[nolistsep]
  \item Arduino
  \item LED
  \item LDR
  \item Jumper Wires
  \item 100K resistor
  \item 220 ohm resistor
  \item Breadboard
\end{itemize}

\section{Circuit}
First of all, you need to connect the LDR to the analogue input pin A0 on the Arduino. You have to use a voltage divider configuration to do this. The connection diagram for the Arduino is given below.\newline
One leg of the LDR is connected to VCC (5V) on the Arduino and the other to the analogue pin A0 on the Arduino. A 100K resistor is also connected to the same leg and grounded.\newline
The power/long leg of the LED is connected straight to pin 11 on the Arduino, the ground/short leg will be connected to a 220 ohm resistor which is then connected to ground.\newline
You can use a breadboard to join the components together.

\begin{figure}[h]
\centering
\includegraphics[width=10cm]{img/ldr-circuit.png}
\caption{Connecting a LDR sensor and LED to an Arduino via a bread-board}
\end{figure}
\newpage

\section{Code}
The first lines of code will let the Arduino know which pins are used in the code.\newline
The light dependent resistor (LDR) will be connected to pin A0 on the Arduino.\newline
The light emitting diode (LED) will be connected to pin 11 on the Arduino.\newline
We then need to create a variable to store the data from the LDR, we will call this LDRvalue.\newline
The very first thing that you do will in the setup function is to begin serial communications, at 9600 bits of data per second, between your board and your computer.\newline
Next, initialise the LDR as an input inside the setup function, you will then need to initialise the LED as an output:

\begin{lstlisting}[label={list:sixth}, style=Arduino, caption=Setting up data connection LDR as an input and LED as an output]
int LDR = A0; //LDR pin is connected to A0
int LED = 11; //LED pin is connected to 11
int LDRvalue = 0; //a place to store the LDR value

void setup() {
  Serial.begin(9600); //set up data connection speed
  pinMode(LED, OUTPUT); //set LED pin 11 as an output
  pinMode(LDR, INPUT); //set LDR pin (A0) as input
}
\end{lstlisting}

\noindent
Inside the main function the first thing we want to do is read the information coming from the LDR. To do this we will read the analogue input and store it to a variable that we defined earlier called LDRvalue. Once we have the value from the LDR, we can then use it to control the brightness of the LED. Instead of using analogue read, we use write for the LED as it's an output.\newline
To be able to read the information from the computer, we need to print the information stored in the LDRvalue variable. To do this, we need to use the serial line to print to the `Serial Monitor'.\newline
You should expect to see values between 0 and 1023 on the computer when you cover and uncover the LDR. The brightness of the LED should also change slightly.


\begin{lstlisting}[label={list:first}, style=Arduino, caption=reading and printing LDR values whilst lighting LED]
void loop() { // the loop function runs over and over again forever
  LDRvalue = analogRead(LDR); //LDRvalue will equal to values from the LDR pin
  analogWrite(LED, LDRvalue); //LDRvalue will determine intensity of LED brightness
  Serial.print("LDR reading is: ");//print out everything between the quotes
  Serial.println(LDRvalue);//print out data in LDRvalue
  delay(10);//delay the code for 10 milliseconds so we can keep up with the readings
}

\end{lstlisting}


\begin{lstlisting}[label={list:tenth}, style=Arduino, caption=Example Code]
int LDR = A0; //LDR pin is connected to A0
int LED = 11; //LED pin is connected to 11
int LDRvalue = 0; //a place to store the LDR value

void setup() {
  Serial.begin(9600); //set up data connection speed
  pinMode(LED, OUTPUT); //set LED pin 11 as an output
  pinMode(LDR, INPUT); //set LDR pin (A0) as input
}

void loop() { // the loop function runs over and over again forever
  LDRvalue = analogRead(LDR);
  Serial.print("LDR reading is: ");//print out everything between the quotation marks
  Serial.println(LDRvalue);//print out data in LDRvalue
  delay(10);//delay the code for 10 milliseconds so we can keep up with the readings
}
\end{lstlisting}

\end{document}
