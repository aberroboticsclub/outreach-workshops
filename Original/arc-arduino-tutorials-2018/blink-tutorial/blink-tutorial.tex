\documentclass[a4paper,11pt]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[UKenglish]{babel}% http://ctan.org/pkg/babel
\usepackage[UKenglish]{isodate}% http://ctan.org/pkg/isodate

\renewcommand\familydefault{\sfdefault}
\usepackage{tgheros}
\usepackage[defaultmono]{droidmono}

\usepackage{amsmath,amssymb,amsthm,textcomp}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{lastpage}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{courier}

\usepackage{geometry}
\geometry{total={210mm,297mm},
left=25mm,right=25mm,%
bindingoffset=0mm, top=20mm,bottom=20mm}


\linespread{1.3}

%\newcommand{\linia}{\rule{\linewidth}{0.5pt}}

% my own titles
%\makeatletter
%\renewcommand{\maketitle}{
%\begin{center}
%\vspace{2ex}
%{\huge \textsc{\@title}}
%\vspace{1ex}
%{\huge \textsc{\@subtitle}}
%\vspace{1ex}
%\\
%\linia\\
%\@author \hfill \@date
%\vspace{4ex}
%\end{center}
%}
%\makeatother
%%%

% custom footers and headers
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Version 1.0 | Release}
\chead{Arduino Tutorials - Blink Tutorial}
\rhead{\today}
%\lfoot{Tomos Fearn (tomos@aberrobotics.club)}
\cfoot{}
\fancyfoot[R]{Page \thepage\ of \pageref{LastPage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%

% code listing settings
\usepackage{listings}

\lstset{%
  language = Octave,
  lineskip={-1.5pt},
  backgroundcolor=\color{white},   
  basicstyle=\footnotesize\ttfamily,       
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                   
  commentstyle=\color{gray},    
  deletekeywords={...},           
  escapeinside={\%*}{*)},          
  extendedchars=true,              
  frame=single,                    
  keepspaces=true,                 
  keywordstyle=\color{orange},       
  morekeywords={*,...},            
  numbers=left,                    
  numbersep=5pt,                   
  numberstyle=\footnotesize\color{gray}, 
  rulecolor=\color{black},         
  rulesepcolor=\color{blue},
  showspaces=false,                
  showstringspaces=false,          
  showtabs=false,                  
  stepnumber=1,                    
  stringstyle=\color{orange},    
  tabsize=2,                       
  title=\lstname,
  emphstyle=\bfseries\color{blue}%  style for emph={} 
} 

%% language specific settings:
\lstdefinestyle{Arduino}{%
    language = Octave,
    keywords={void, int boolean},%                 define keywords
    morecomment=[l]{//},%             treat // as comments
    morecomment=[s]{/*}{*/},%         define /* ... */ comments
    emph={HIGH, INPUT, OUTPUT, LOW}%        keywords to emphasize
}


%%%----------%%%----------%%%----------%%%----------%%%

\begin{document}

\title{Aberystwyth Robotics Club\\Arduino Tutorials\\Blink Tutorial}
%\author{Dr Hannah Dee, Tomos Fearn, Dr Keiran Burrows, Stephen Fearn}
\date{\parbox{\linewidth}{\centering%
  \today\endgraf\bigskip
  Version 1.0 \hspace*{3cm} Release\endgraf\medskip
  %Dept.\ of Physics \endgraf
  %ABC College
  }}



\maketitle
\begin{center}
\includegraphics[width=5cm]{img/logo.png}
\end{center}
\newpage

\begin{center}
\section*{Abstract}
This example shows the simplest thing you can do with an Arduino or Genuino to see physical output: it blinks the on-board LED.
\end{center}



\section{Hardware Required}
\begin{itemize}[nolistsep]
  \item Arduino or Genuino Board optional
  \item LED
  \item 220 ohm resistor
\end{itemize}

\section{Circuit}
To build the circuit, connect one end of the resistor to Arduino pin 13. Connect the long leg of the LED (the positive leg, called the anode) to the other end of the resistor. Connect the short leg of the LED (the negative leg, called the cathode) to the Arduino GND, as shown in the diagram and the schematic below.\newline
Most Arduino boards already have an LED attached to pin 13 on the board itself. If you run this example with no hardware attached, you should see that LED blink.\newline
The value of the resistor in series with the LED may be of a different value than 220 ohm; the LED will lit up also with values up to 1k ohm.

\begin{figure}[h]
\centering
\includegraphics[width=8cm]{img/circuit.png}\includegraphics[width=8cm]{img/schematic.png}
\caption{Circuit and Schematic https://www.arduino.cc/en/tutorial/blink}
\end{figure}
\newpage



\section{Code}
After you build the circuit plug your Arduino or Genuino board into your computer, start the Arduino Software (IDE) and enter the code below \footnote{https://www.arduino.cc/en/tutorial/blink}.\newline
You may also load it from the menu File/Examples/01.Basics/Blink . The first thing you do is to initialize pin 13 as an output pin with the line.

\begin{lstlisting}[label={list:first}, style=Arduino, caption=Declaring Variables]
pinMode(13, OUTPUT);
\end{lstlisting}

\noindent
In the main loop, you turn the LED on with the line:

\begin{lstlisting}[label={list:first}, style=Arduino, caption=Turning on the LED]
digitalWrite(13, HIGH);
\end{lstlisting}

\noindent
This supplies 5 volts to the LED anode. That creates a voltage difference across the pins of the
LED, and lights it up. Then you turn it off with the line:

\begin{lstlisting}[label={list:first}, style=Arduino, caption=Turning off the LED]
digitalWrite(13, LOW);
\end{lstlisting}

\noindent
That takes pin 13 back to 0 volts, and turns the LED off. In between the on and the off, you want enough time for a person to see the change, so the delay() commands tell the board to do nothing for 1000 milliseconds, or one second. When you use the delay() command, nothing else happens for that amount of time.\newline
Once you’ve understood the basic examples, check out the BlinkWithoutDelay example to learn how to create a delay while doing other things.

\begin{lstlisting}[label={list:second}, style=Arduino, caption=Example Code]
void setup () {
    pinMode(13, OUTPUT); // initialize digital pin 13 as an output .
}
void loop () { // the loop function runs over and over again forever
    digitalWrite(13, HIGH); // turn the LED on
    delay(1000); // wait for a second
    digitalWrite(13, LOW); // turn the LED off
    delay(1000); // wait for a second
}
\end{lstlisting}






\end{document}
