\documentclass[a4paper,11pt]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[UKenglish]{babel}% http://ctan.org/pkg/babel
\usepackage[UKenglish]{isodate}% http://ctan.org/pkg/isodate

\renewcommand\familydefault{\sfdefault}
\usepackage{tgheros}
\usepackage[defaultmono]{droidmono}

\usepackage{amsmath,amssymb,amsthm,textcomp}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{lastpage}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{courier}

\usepackage{geometry}
\geometry{total={210mm,297mm},
left=25mm,right=25mm,%
bindingoffset=0mm, top=20mm,bottom=20mm}


\linespread{1.3}

%\newcommand{\linia}{\rule{\linewidth}{0.5pt}}

% my own titles
%\makeatletter
%\renewcommand{\maketitle}{
%\begin{center}
%\vspace{2ex}
%{\huge \textsc{\@title}}
%\vspace{1ex}
%{\huge \textsc{\@subtitle}}
%\vspace{1ex}
%\\
%\linia\\
%\@author \hfill \@date
%\vspace{4ex}
%\end{center}
%}
%\makeatother
%%%

% custom footers and headers
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Version 1.0 | Release}
\chead{Arduino Tutorials - Ultrasonic Tutorial}
\rhead{\today}
%\lfoot{Tomos Fearn (tomos@aberrobotics.club)}
\cfoot{}
\fancyfoot[R]{Page \thepage\ of \pageref{LastPage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%

% code listing settings
\usepackage{listings}

\lstset{%
  language = Octave,
  lineskip={-1.5pt},
  backgroundcolor=\color{white},   
  basicstyle=\footnotesize\ttfamily,       
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                   
  commentstyle=\color{gray},    
  deletekeywords={...},           
  escapeinside={\%*}{*)},          
  extendedchars=true,              
  frame=single,                    
  keepspaces=true,                 
  keywordstyle=\color{orange},       
  morekeywords={*,...},            
  numbers=left,                    
  numbersep=5pt,                   
  numberstyle=\footnotesize\color{gray}, 
  rulecolor=\color{black},         
  rulesepcolor=\color{blue},
  showspaces=false,                
  showstringspaces=false,          
  showtabs=false,                  
  stepnumber=1,                    
  stringstyle=\color{orange},    
  tabsize=2,                       
  title=\lstname,
  emphstyle=\bfseries\color{blue}%  style for emph={} 
} 

%% language specific settings:
\lstdefinestyle{Arduino}{%
    language = Octave,
    keywords={void, int boolean},%                 define keywords
    morecomment=[l]{//},%             treat // as comments
    morecomment=[s]{/*}{*/},%         define /* ... */ comments
    emph={HIGH, INPUT, OUTPUT, LOW}%        keywords to emphasize
}


%%%----------%%%----------%%%----------%%%----------%%%

\begin{document}

\title{Aberystwyth Robotics Club\\Arduino Tutorials\\Ultrasonic Tutorial}
%\author{Tomos Fearn (tomos@aberrobotics.club)}
\date{\parbox{\linewidth}{\centering%
  \today\endgraf\bigskip
  Version 1.0 \hspace*{3cm} Release\endgraf\medskip
  %Dept.\ of Physics \endgraf
  %ABC College
  }}



\maketitle
\begin{center}
\includegraphics[width=5cm]{img/logo.png}
\end{center}
\newpage

\begin{center}
\section*{Abstract}
This example shows you how to find the distance from an obstacle using ultrasonic sensors, for now we will return the values from the ultrasonic to the serial monitor on the Arduino
\end{center}



\section{Hardware Required}
\begin{itemize}[nolistsep]
  \item Arduino or Genuino Board
  \item HC-SR04 Ultrasonic
  \item Jumper Cables
  \item Optional breadboard
\end{itemize}

\section{Circuit}
Ultrasonics work by sending out pulses of sound, and then counting how long it takes for those pulses to come back. So a Ultrasonic sensor actually has two parts: a transmitter and a receiver.\newline
If you look at the sensor it has two round bits, labelled T for transmitter and R for receiver. (In electronics things which both transmit and receive signals are often called “Transceivers” so this is really a Ultrasonic transceiver.)\newline
The Ultrasonic sensor has 4 wires labelled Vcc, Trig, Echo and Gnd.\newline
These are for the voltage, the trigger of the sonar, the echo detection signal, and the ground.

\begin{itemize}[nolistsep]
  \item The first of these (voltage, or Vcc) needs to be wired up to the 5v output of your Arduino.
  \item The trigger or Trig needs to be wired up to a data pin on the Arduino - let's wire it up to number 2.
  \item The echo detection signal also goes to a data pin on the Arduino - let's wire it up to number 3
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=8cm]{img/circuit.png}\includegraphics[width=6cm]{img/schematic.png}
\caption{Circuit and Schematic}
\end{figure}
\newpage



\section{Sending out a pulse and timing it coming back}
Remember that all Arduino programs have two parts: the setup which happens once, and is used to set up anything that you need to use in the program. This is where you tell the Arduino which components are wired up to which pin, and stuff like that. There is also a loop, and the loop is repeated again and again until the Arduino is turned off (or runs out of battery power).\newline
In the setup function here we need to tell the Arduino which pin has the Trig connection, and which pin has the Echo connection.\newline
We are going to use the trigger pin to send a message to the sensor from the Arduino (so that is an OUTPUT pin), but we are going to use the Echo pin to read a message from the sensor (so that is an INPUT pin.)

\section{Code}
After you build the circuit plug your Arduino or Genuino board into your computer, start the Arduino Software (IDE) and enter the code below\footnote{https://create.arduino.cc/projecthub/Isaac100/getting-started-with-the-hc-sr04-ultrasonic-sensor-036380}.

\begin{lstlisting}[label={list:first}, style=Arduino, caption=Setup Functions]
void setup () {
    Serial.begin(9600);
    pinMode(2, OUTPUT);
    pinMode(3, INPUT);
}
\end{lstlisting}

As well as telling the program what’s connected to what pin, it also sets up the serial monitor so we are ready to start monitoring stuff.\newline
Now we have set up the program we need to trigger the sensor to send out a signal, then read the signal coming back from the sensor. We can do this as follows:

\begin{lstlisting}[label={list:first}, style=Arduino, caption=Example Code]
void loop () {
    long duration;
    digitalWrite(2, HIGH);
    delayMicroseconds(10);
    digitalWrite(2, LOW);
    duration = pulseIn(3, HIGH);
    Serial.print(duration);
    Serial.println(" : duration ");
}
\end{lstlisting}

There is a lot going on in this program.

\begin{itemize}[nolistsep]
  \item First we setup a variable called duration to hikd the length of time between the sound going out and the sound bouncing back.
  \item Then we trigger a quick pulse of sound (by writing HIGH to the trigger pin) - we only do this for a tiny amount of time (10 microseconds) before turning it off again by writing LOW to the trigger pin.
  \item We then capture the time from the pulse being sent out to coming back by looking for a HIGH pulseIn on the echo pin.
  \item Finally we print the duration out to the serial monitor.
\end{itemize}

Type this program in, test it by clicking the tick, then put it on the Arduino using the arrow button.\newline

To open the serial monitor, click on the button that looks a bit like a magnifying glass in the top right corner of the Arduino IDE window (it will say “Serial Monitor” when you hover over it). That will open up a new window which contains the stuff you have printed to Serial.\newline

You should see something like this:

\begin{lstlisting}[label={list:first}, style=Arduino, caption=Example Output]
386: duration
417: duration
381: duration
\end{lstlisting}

These are measurements of how long it took (in microseconds) for the sound to bounce off a thing in the world and then back to the ultrasonic sensor.

\section{Going from a measurement of time to a measurement of distance}

Set up a barrier near your computer, and get hold of a ruler.\newline
Measure the distance from the sensor to the barrier, and fill in a table of measurements from the serial monitor.\newline
If the barrier is 10cm from the sensor, how long does it take the sound to travel?\newline
If the barrier is 10cm from the sensor, how long does it take the sound to travel?\newline
Take some more measurements. Can you come up with a way of converting ``sound-travel-time'' into centimetres?\newline
If you can come up with a useful conversion system, can you then get that into your program - that is, can you work out how to get your program to give an output in centimetres? Give it a go, and then test your ultrasonic ruler against a real ruler.


\end{document}
