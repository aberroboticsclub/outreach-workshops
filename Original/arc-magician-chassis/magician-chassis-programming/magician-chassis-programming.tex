% Template for use of Tomos Fearn and Aberystwyth Robotics Club
% Author:   Tomos Fearn
%           tomos@fearn.org.uk
%           http://tomos.fearn.org.uk

\documentclass[a4paper,11pt]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage[UKenglish]{babel}% http://ctan.org/pkg/babel
\usepackage[UKenglish]{isodate}% http://ctan.org/pkg/isodate

\renewcommand\familydefault{\sfdefault}
\usepackage{tgheros}
\usepackage[defaultmono]{droidmono}

\usepackage{amsmath,amssymb,amsthm,textcomp}
\usepackage{enumerate}
\usepackage{enumitem}
\usepackage{lastpage}
\usepackage{multicol}
\usepackage{tikz}
\usepackage{courier}

\usepackage{array}

\usepackage{geometry}
\geometry{total={210mm,297mm},
left=25mm,right=25mm,%
bindingoffset=0mm, top=20mm,bottom=20mm}


\linespread{1.3}

%\newcommand{\linia}{\rule{\linewidth}{0.5pt}}

% my own titles
%\makeatletter
%\renewcommand{\maketitle}{
%\begin{center}
%\vspace{2ex}
%{\huge \textsc{\@title}}
%\vspace{1ex}
%{\huge \textsc{\@subtitle}}
%\vspace{1ex}
%\\
%\linia\\
%\@author \hfill \@date
%\vspace{4ex}
%\end{center}
%}
%\makeatother
%%%

% custom footers and headers
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{Version 1.0 | Release}
\chead{Magician Chassis - Programming Instructions}
\rhead{7th February 2018}
%\lfoot{Tomos Fearn (tomos@aberrobotics.club)}
\cfoot{}
\fancyfoot[R]{Page \thepage\ of \pageref{LastPage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
%

% code listing settings
\usepackage{listings}

\lstset{%
  language = Octave,
  lineskip={-1.5pt},
  backgroundcolor=\color{white},   
  basicstyle=\footnotesize\ttfamily,       
  breakatwhitespace=false,         
  breaklines=true,                 
  captionpos=b,                   
  commentstyle=\color{gray},    
  deletekeywords={...},           
  escapeinside={\%*}{*)},          
  extendedchars=true,              
  frame=single,                    
  keepspaces=true,                 
  keywordstyle=\color{orange},       
  morekeywords={*,...},            
  numbers=left,                    
  numbersep=5pt,                   
  numberstyle=\footnotesize\color{gray}, 
  rulecolor=\color{black},         
  rulesepcolor=\color{blue},
  showspaces=false,                
  showstringspaces=false,          
  showtabs=false,                  
  stepnumber=1,                    
  stringstyle=\color{orange},    
  tabsize=2,                       
  title=\lstname,
  emphstyle=\bfseries\color{blue}%  style for emph={} 
} 

%% language specific settings:
\lstdefinestyle{Arduino}{%
    language = Octave,
    keywords={void, int boolean},%                 define keywords
    morecomment=[l]{//},%             treat // as comments
    morecomment=[s]{/*}{*/},%         define /* ... */ comments
    emph={HIGH, INPUT, OUTPUT, LOW}%        keywords to emphasize
}


%%%----------%%%----------%%%----------%%%----------%%%

\begin{document}

\title{Aberystwyth Robotics Club\\Magician Chassis\\Programming Instructions}
%\author{Tomos Fearn (tomos@aberrobotics.club)}
\date{\parbox{\linewidth}{\centering%
  7th February 2018\endgraf\bigskip
  Version 1.0 \hspace*{3cm} Release\endgraf\medskip
  %Model:\ Transparent Chassis \endgraf
  }}



\maketitle
\begin{center}
\includegraphics[width=5cm]{img/logo.png}
\end{center}
\newpage

\section{Motor Instructions}
The motors on the Magician Chassis are controlled by a h-bridge motor controller and has 2 channels, which allows the control of two DC motors.

\begin{lstlisting}[label={list:first}, style=Arduino, caption=Declaring Variables]
byte leftForward = 5;
byte leftReverse = 6;
byte rightForward = 9;
byte rightReverse = 10;
\end{lstlisting}

You will need to define the motors inside the setup function as output devices:

\begin{lstlisting}[label={list:second}, style=Arduino, caption=Setup Function]
void setup() {
  pinMode(leftForward, OUTPUT); //set leftForward as output
  pinMode(leftReverse, OUTPUT); //set leftReverse as output
  pinMode(rightForward, OUTPUT); //set rightForward as output
  pinMode(rightReverse, OUTPUT); //set rightReverse as output
}
\end{lstlisting}

\subsection{Functions}
You need to create 5 functions which give instructions to the robot, create the functions below:
``A function is to divide up your code into chunks, this will make it easier to create instructions to the robot and prevents code duplication!''

\begin{itemize}[nolistsep]
  \item robotForwards()
  \item robotLeft()
  \item robotRight()
  \item robotReverse()
  \item robotStop()
\end{itemize}

\noindent
See below the example of creating a function.\newline
\texttt{digitalWrite} means we want to send a digital signal to whatever is inside this function.\newline
\texttt{leftForward} is the pin we would like the \texttt{digitalWrite} to act on.\newline
\texttt{HIGH} is what we would like the pin to do. Digital signals work on 1s and 0s, 1 being on and 0 being off. In our case, 1 will be go and 0 will be stop.

\begin{lstlisting}[label={list:fourth}, style=Arduino, caption=Sending signals to the motor controller to power the motors]
void robotForwards(){
    digitalWrite(leftForward, HIGH); //send the left motor forwards
    digitalWrite(leftReverse, LOW); //prevent the left motor reversing
    digitalWrite(rightForward, HIGH); //send the right motor forwards
    digitalWrite(rightReverse, LOW); //prevent the right motor reversing
}
void robotLeft(){
    digitalWrite(leftForward, LOW); //prevent the left motor going forwards
    digitalWrite(leftReverse, HIGH); //reverse the left motor
    digitalWrite(rightForward, HIGH); //send the right motor forwards
    digitalWrite(rightReverse, LOW); //prevent the right motor reversing
}
\end{lstlisting}

\newpage
\begin{center}
\begin{tabular}{|p{5cm}|p{5cm}|}
  \hline
  Arduino Signal & Motor Action \\
  \hline
  \texttt{HIGH} & GO\\
  \hline
  \texttt{LOW} & STOP\\
  \hline
\end{tabular}
\end{center}

\noindent
Once you create the instructions in the functions you can call the function in void loop(), try all the functions you have created and make the robot move around:

\begin{lstlisting}[label={list:fifth}, style=Arduino, caption=Loop Function]
void loop(){  
  robotForwards();
}
\end{lstlisting}


\subsection{Behaviours}
Can you use these functions and the delay function to navigate a maze?
\begin{lstlisting}[label={list:fifth}, style=Arduino, caption=Example Code]
void loop(){  
  robotForwards(); //move robot forwards
  delay(1000) //1000 milliseconds = 1 second
  robotLeft(); //move robot left
  delay(500) //500 milliseconds = 0.5 second
}
\end{lstlisting}
\newpage



\section{Servo}
A servo is a small motor which, using a potentiometer, can measure the amount the shaft has turned. In our case we will want to know how far the servo has turned in degrees.

\subsection{Hardware Required}
\begin{itemize}[nolistsep]
  \item Arduino or Genuino Board
  \item Servo Motor
  \item Jumper Wires 
\end{itemize}

\subsection{Circuit}
Servo motors have three wires: power, ground, and signal. The power wire is typically red, and should be connected to the 5V pin on the Arduino or Genuino board. The ground wire is typically black or brown and should be connected to a ground pin on the board. The signal pin is typically yellow, orange or white and should be connected to pin 11 on the board. 

\begin{figure}[h]
\centering
\includegraphics[width=11cm]{img/servo-circuit.png}
\caption{Connecting a servo to an Arduino}
\end{figure}

\newpage
\subsection{Code}
First we need to import a library for the Arduino, this stores some extra code so we don't have to write as much!
\begin{lstlisting}[label={list:sixth}, style=Arduino, caption=Importing an Arduino Library]
#include <Servo.h>
\end{lstlisting}

\noindent
We then need to create a servo object, the first word calls the Servo library and myservo is what we will call the servo later on in the code:
\begin{lstlisting}[label={list:sixth}, style=Arduino, caption=Creating a servo object]
Servo myservo;
\end{lstlisting}

\noindent
We then need to create a variable to store the position of our servo, we will set the servo start point as 0 for now:
\begin{lstlisting}[label={list:sixth}, style=Arduino, caption=Creating a storage variable]
int pos = 0;
\end{lstlisting}

\noindent
In the setup function we need to attach myservo object to pin 11:
\begin{lstlisting}[label={list:eleventh}, style=Arduino, caption=Setup Function]
void setup() {
  myservo.attach(11);
}
\end{lstlisting}

\noindent
The next step is to move the servo. The first part of the code is the most important part.\newline
Below is some pseudo code to explain what is happening.\newline
We start the position of the servo at 0 degrees, if the position is less than or equal to 180 degrees, then add one degree to the position variable.\newline
Now we have the value 1 in variable `pos', myservo will move 1 degree and wait 15 milliseconds.\newline
Once it has moved 1 degree and waited, the code returns to the top and adds an extra degree to the position variable and loops this over and over until the position variable reaches 180.\newline
Once the servo reaches 180 degrees, it jumps outside the for loop, then moves along to the next for loop where it does exactly the same action but in reverse from 180 to 0.

\noindent
This can be done using the code below:
\begin{lstlisting}[label={list:eleventh}, style=Arduino, caption=Loop Function and Example Code]
void loop() {
  for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos); // tell servo to go to position in variable `pos'
    delay(15); // waits 15ms for the servo to reach the position
  }
  for (pos = 180; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos); // tell servo to go to position in variable `pos'
    delay(15); // waits 15ms for the servo to reach the position
  }
}
\end{lstlisting}
\newpage



\section{Light Dependant Resistors Instructions}
In order to detect the intensity of light or darkness, we use a sensor called an LDR (Light Dependent Resistor). The LDR is a special type of resistor which allows higher voltages to pass through it (low resistance) whenever there is a high intensity of light, and passes a low voltage (high resistance) whenever it is dark.


\subsection{Hardware Required}
\begin{itemize}[nolistsep]
  \item Arduino
  \item LED
  \item LDR
  \item Jumper Wires
  \item 100K resistor
  \item 220 ohm resistor
  \item Breadboard
\end{itemize}

\subsection{Circuit}
First of all, you need to connect the LDR to the analogue input pin A0 on the Arduino. You have to use a voltage divider configuration to do this. The connection diagram for the Arduino is given below.\newline
One leg of the LDR is connected to VCC (5V) on the Arduino and the other to the analogue pin A0 on the Arduino. A 100K resistor is also connected to the same leg and grounded.\newline
The power/long leg of the LED is connected straight to pin 13 on the Arduino, the ground/short leg will be connected to a 220 ohm resistor which is then connected to ground.\newline
You can use a breadboard to join the components together.

\begin{figure}[h]
\centering
\includegraphics[width=10cm]{img/ldr-circuit.png}
\caption{Connecting a LDR sensor and LED to an Arduino via a bread-board}
\end{figure}

\subsection{Code}
The first lines of code will let the Arduino know which pins are used in the code.\newline
The light dependent resistor (LDR) will be connected to pin A0 on the Arduino.\newline
The light emitting diode (LED) will be connected to pin 13 on the Arduino.\newline
We then need to create a variable to store the data from the LDR, we will call this LDRvalue.\newline
The very first thing that you do will in the setup function is to begin serial communications, at 9600 bits of data per second, between your board and your computer.\newline
Next, initialise the LDR as an input inside the setup function, you will then need to initialise the LED as an output:

\begin{lstlisting}[label={list:sixth}, style=Arduino, caption=Setting up data connection LDR as an input and LED as an output]
int LDR = A0; //LDR pin is connected to A0
int LED = 13; //LED pin is connected to 13
int LDRvalue = 0; //a place to store the LDR value

void setup() {
  Serial.begin(9600); //set up data connection speed
  pinMode(LED, OUTPUT); //set LED pin 13 as an output
  pinMode(LDR, INPUT); //set LDR pin (A0) as input
}
\end{lstlisting}

\noindent
Inside the main function the first thing we want to do is read the information coming from the LDR. To do this we will read the analogue input and store it to a variable that we defined earlier called LDRvalue. Once we have the value from the LDR, we can then use it to control the brightness of the LED. Instead of using analogue read, we use write for the LED as it's an output.\newline
To be able to read the information from the computer, we need to print the information stored in the LDRvalue variable. To do this, we need to use the serial line to print to the `Serial Monitor'.\newline
You should expect to see values between 0 and 1023 on the computer when you cover and uncover the LDR. The brightness of the LED should also change slightly.


\begin{lstlisting}[label={list:first}, style=Arduino, caption=reading and printing LDR values whilst lighting LED]
void loop() { // the loop function runs over and over again forever
  LDRvalue = analogRead(LDR); //LDRvalue will equal to values from the LDR pin
  analogWrite(LED, LDRvalue); //LDRvalue will determine intensity of LED brightness
  Serial.print("LDR reading is: ");//print out everything between the quotes
  Serial.println(LDRvalue);//print out data in LDRvalue
  delay(10);//delay the code for 10 milliseconds so we can keep up with the readings
}

\end{lstlisting}


\begin{lstlisting}[label={list:tenth}, style=Arduino, caption=Example Code]
int LDR = A0; //LDR pin is connected to A0
int LED = 13; //LED pin is connected to 13
int LDRvalue = 0; //a place to store the LDR value

void setup() {
  Serial.begin(9600); //set up data connection speed
  pinMode(LED, OUTPUT); //set LED pin 13 as an output
  pinMode(LDR, INPUT); //set LDR pin (A0) as input
}

void loop() { // the loop function runs over and over again forever
  LDRvalue = analogRead(LDR);
  Serial.print("LDR reading is: ");//print out everything between the quotation marks
  Serial.println(LDRvalue);//print out data in LDRvalue
  delay(10);//delay the code for 10 milliseconds so we can keep up with the readings
}
\end{lstlisting}

\subsection{Behaviours}
Can you make the robot follow a torch? You may consider using ``if'' statements to do this.

\newpage




\section{Ultrasonic Instructions}
This example shows you how to find the distance from an obstacle using ultrasonic sensors, for now we will return the values from the ultrasonic to the serial monitor on the Arduino.

\subsection{Hardware Required}
\begin{itemize}[nolistsep]
  \item Arduino
  \item HC-SR04 Ultrasonic
  \item Jumper Wires
  \item Optional breadboard
\end{itemize}

\subsection{Circuit}
Ultrasonics work by sending out pulses of sound, and then counting how long it takes for those pulses to come back. So a Ultrasonic sensor actually has two parts: a transmitter and a receiver
If you look at the sensor it has two round bits, labelled T for transmitter and R for receiver. (In electronics things which both transmit and receive signals are often called “Transceivers” so tgus is really a Ultrasonic transceiver.)\newline
The Ultrasonic sensor has 4 wires labelled Vcc, Trig, Echo and Gnd.\newline
These are for the voltage, the trigger of the sonar, the echo detection signal, and the ground.
\begin{itemize}[nolistsep]
  \item The first of these (voltage, or Vcc) needs to be wired up to the 5v output of your Arduino.
  \item The trigger or Trig needs to be wired up to a data pin on the Arduino - let’s wire it up to number 3.
  \item The echo detection signal also goes to a data pin on the Arduino – let’s wire it up to number 4.
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=12cm]{img/us-circuit.png}
\caption{Connecting a Ultrasonic sensor to an Arduino}
\end{figure}

\newpage

\subsection{Sending out a pulse and timing it coming back}
Remember that all Arduino programs have two parts: the setup which happens once, and is used to set up anything that you need to use in the program. This is where you tell the Arduino which components are wired up to which pin, and stuff like that. There is also a loop, and the loop is repeated again and again until the Arduino is turned off (or runs out of battery power).\newline
In the setup function here we need to tell the Arduino which pin has the Trig connection, and which pin has the Echo connection.\newline
We are going to use the trigger pin to send a message to the sensor from the Arduino (so that is an OUTPUT pin), but we are going to use the Echo pin to read a message from the sensor (so that is an INPUT pin.)

\subsection{Code}
After you build the circuit plug your Arduino board into your computer, start the Arduino Software (IDE) and enter the code below.

\begin{lstlisting}[label={list:eleventh}, style=Arduino, caption=Setup Function]
int trigger = 3;
int echo = 4;
void setup () {
  Serial.begin(9600);
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
}
\end{lstlisting}

\noindent
As well as telling the program what’s connected to what pin, it also sets up the serial monitor so we are ready to start monitoring stuff.\newline
Now we have set up the program, we need to trigger the sensor to send out a signal, then read the signal coming back from the sensor. We can do this as follows:

\begin{lstlisting}[label={list:twelfth}, style=Arduino, caption=Loop Function]
void loop () {
  long duration;
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  duration = pulseIn(echo, HIGH);
  Serial.print(duration);
  Serial.println(" : duration ");
}
\end{lstlisting}

There is a lot going on in this program.

\begin{itemize}[nolistsep]
  \item First we setup a variable called duration to hold the length of time between the sound going out and the sound bouncing back.
  \item When we trigger a quick pulse of sound (by writing HIGH to the trigger pin) - we only do this for a tiny amount of time (10 microseconds) before turning it off again by writing LOW to the trigger pin.
  \item We then capture the time from the pulse being sent out to coming back by looking for a HIGH pulseIn on the echo pin.
  \item Finally we print the duration out to the serial monitor.
\end{itemize}

\noindent
Type this program in, test it by clicking the tick, then put it on the Arduino using the arrow button.
To open the serial monitor, click on the button that looks a bit like a magnifying glass in the top right corner of the Arduino IDE window (it will say ``Serial Monitor'' when you hover over it). That will open up a new window which contains the stuff you have printed to Serial.\newline

\noindent
You should see something like this:
\begin{lstlisting}[label={list:thirteenth}, style=Arduino, caption=Example Output]
386: duration
417: duration
381: duration
\end{lstlisting}

\noindent
These are measurements of how long it took (in microseconds) for the sound
to bounce off a thing in the world and then back to the ultrasonic sensor.

\subsection{Going from a measurement of time to a measurement of distance}
Set up a barrier near your computer, and get hold of a ruler.\newline
Measure the distance from the sensor to the barrier, and fill in a table of measurements from the serial monitor.\newline
If the barrier is 10cm from the sensor, how long does it take the sound to travel?\newline
If the barrier is 10cm from the sensor, how long does it take the sound to travel?\newline
Take some more measurements. Can you come up with a way of converting ``sound-travel-time'' into centimetres?\newline
If you can come up with a useful conversion system, can you then get that into your program - that is, can you work out how to get your program to give an output in centimetres? Give it a go, and then test your ultrasonic ruler against a real ruler.

\subsection{Behaviours}
Can you avoid obstacles using the ultraonic sensor whilst the robot is driving?
\newpage



\end{document}
