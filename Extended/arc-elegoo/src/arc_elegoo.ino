
#include <IRremote.h>
#include <Servo.h>  //servo library
Servo myservo;      // create servo object to control servo

#define LeftEnable 5
#define LeftForward 7
#define LeftReverse 8

#define RightEnable 6
#define RightForward 11
#define RightReverse 9

#define LineSensorRight 10
#define LineSensorMiddle 4
#define LineSensorLeft 2

#define RECV_PIN  12        //Infrared signal receiving pin
IRrecv irrecv(RECV_PIN);
decode_results results;

////////// IR REMOTE CODES //////////
#define Fa 16736925  // FORWARD
#define Ba 16754775  // BACK
#define La 16720605  // LEFT
#define Ra 16761405  // RIGHT
#define Sa 16712445  // STOP

#define Fb 5316027     // FORWARD
#define Bb 2747854299  // BACK
#define Lb 1386468383  // LEFT
#define Rb 553536955   // RIGHT
#define Sb 3622325019  // STOP

#define OneA 16738455   // Line follow
#define OneB 3238126971
#define TwoA 16750695   // obstacle avoid
#define TwoB 2538093563
#define FourA 16724175  // steer
#define FourB 2534850111

unsigned long val;
unsigned long preMillis;

int Echo = A4;
int Trig = A5;


int carSpeed=150;
int rightDistance = 0, leftDistance = 0, middleDistance = 0;

void setup() {
  // put your setup code here, to run once:
  
  Serial.begin(9600);
  
  pinMode(LeftEnable, OUTPUT);
  pinMode(LeftForward, OUTPUT);
  pinMode(LeftReverse, OUTPUT);

  pinMode(RightEnable, OUTPUT);
  pinMode(RightForward, OUTPUT);
  pinMode(RightReverse, OUTPUT);

  pinMode(LineSensorRight, INPUT);
  pinMode(LineSensorMiddle, INPUT);
  pinMode(LineSensorLeft, INPUT);

  pinMode(Echo, INPUT);    
  pinMode(Trig, OUTPUT);
  myservo.attach(3);  // attach servo on pin 3 to servo object

  analogWrite(LeftEnable, carSpeed); //Enable left motor
  analogWrite(RightEnable, carSpeed); //Enable right motor

  irrecv.enableIRIn(); // Start the receiver
}

void loop() {
  // put your main code here, to run repeatedly:
// lineFollowing();
// servoTest();

//  obstacleAvoid();


  if (irrecv.decode(&results)){ 
    //preMillis = millis();
    val = results.value;
    Serial.println(val);
    irrecv.resume();

    
    }
  
  
  switch(val){
      case OneA: 
      case OneB: lineFollowing(); break;
      case TwoA: 
      case TwoB: obstacleAvoid(); break;
      case FourA: 
      case FourB: remoteSteer(); break;
      default: break;
  }

  delay(500);

  
}

bool readRight() {
  bool reading;
  Serial.print("Right ");
  reading=digitalRead(LineSensorRight);
  Serial.println(reading);
  return reading;
}

bool readMiddle() {
  bool reading;
  Serial.print("Middle ");
  reading=digitalRead(LineSensorMiddle);
  Serial.println(reading);
  return reading;
}

bool readLeft() { // define a function which returns a value
  bool reading;   // temporary variable
  Serial.print("Left ");  // print a message to the serial monitor so we know which sensor it is
  reading=digitalRead(LineSensorLeft);  // obtain a reading from the sensor, either 0 ir 1
  Serial.println(reading);  // print the reading alongside the message above, followed by a line break (println)
  return reading; // return the reading for use elsewhere
}


void setLeftForward() {
  digitalWrite(LeftForward, HIGH);      
  digitalWrite(LeftReverse, LOW); //Left wheel turning forwards
}

void setLeftReverse() {
  digitalWrite(LeftForward, LOW);      
  digitalWrite(LeftReverse, HIGH);//Left wheel turning backwards
}

void setLeftStop() {
  digitalWrite(LeftForward, LOW);      
  digitalWrite(LeftReverse, LOW); //Left wheel stopped
}

void setRightForward() {
  digitalWrite(RightForward, HIGH);      
  digitalWrite(RightReverse, LOW); //Right wheel turning forwards
}

void setRightReverse() {
  digitalWrite(RightForward, LOW);      
  digitalWrite(RightReverse, HIGH);//Right wheel turning backwards
}

void setRightStop() {
  digitalWrite(RightForward, LOW);      
  digitalWrite(RightReverse, LOW); //Right wheel stopped
}

void setForward() {
  setLeftForward();
  setRightForward();
}

void setReverse() {
  setLeftReverse();
  setRightReverse();
}

void setStop() {
  setLeftStop();
  setRightStop();
}

//Ultrasonic distance measurement Sub function
int Distance_test() {
  digitalWrite(Trig, LOW);   
  delayMicroseconds(2);
  digitalWrite(Trig, HIGH);  
  delayMicroseconds(20);
  digitalWrite(Trig, LOW);   
  float Fdistance = pulseIn(Echo, HIGH);  
  Fdistance= Fdistance / 58;       
  return (int)Fdistance;
}  



void dance(){
  setLeftForward();
  delay(500);             //pause 500ms
  setLeftStop();
  delay(500);
  setLeftReverse();
  delay(500);
  setLeftStop();
  delay(500);
  
  setRightForward();
  delay(500);
  setRightStop();
  delay(500);
  setRightReverse();
  delay(500);
  setRightStop();
  delay(500);

  setForward();
  delay(500);
  setStop();
  delay(500);
  setReverse();
  delay(500);
  setStop();
  delay(500);
}

void lineFollowing(){
  readRight();
  readMiddle();
  readLeft();
  Serial.println();
  delay(500);

  setStop();
  if(readLeft()){
    setLeftForward();
  }
  else if(readRight()){
    setRightForward();
  }
  else if(readMiddle()){
    setForward();
  }
  delay(20);
}

void servoTest(){
  for (int pos = 30; pos <= 150; pos += 1) { // goes from 0 degrees to 180 degrees
                      // in steps of 1 degree
    myservo.write(pos); // tell servo to go to position in variable "pos"
    delay(20); // waits 15ms for the servo to reach the position
  }
  for (int pos = 150; pos >= 30; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos); // tell servo to go to position in variable "pos"
    delay(20); // waits 15ms for the servo to reach the position
  }
}

void obstacleAvoid(){
    myservo.write(90);  //setservo position according to scaled value
    delay(100); 
    middleDistance = Distance_test();
    Serial.print("Middle dist: ");
    Serial.println(middleDistance);

    if(middleDistance <= 20) {     
      setStop();
      delay(500);
                         
      myservo.write(20);          
      delay(1000);      
      rightDistance = Distance_test();
      delay(500);

      myservo.write(160);          
      delay(1000);      
      leftDistance = Distance_test();
      delay(500);

      myservo.write(90);
      delay(500);

      if((rightDistance<=20) && (leftDistance <= 20)){
        setRightForward();
        setLeftReverse();
        delay(360);
      }

      else if(rightDistance > leftDistance)
      {
        setRightForward();
        delay(360);
      }
      else if(leftDistance > rightDistance)
      {
        setLeftForward();
        delay(360);
      }
      
      

    }
    else
    {
      setForward();
    }
}

void remoteSteer()
{
  while(!(val==Sa || val==Sb))
    {
     if (irrecv.decode(&results)){ 
      preMillis = millis();
      val = results.value;
      Serial.println(val);
      irrecv.resume();
  
      switch(val){
        case Fa: 
        case Fb: setForward(); break;
        case Ba: 
        case Bb: setReverse(); break;
        case La: 
        case Lb: setLeftForward(); break;
        case Ra: 
        case Rb: setRightForward();break;
        case Sa: 
        case Sb: setStop(); break;
        default: break;
      }
    }
    else{
      if(millis() - preMillis > 500){
        setStop();
        preMillis = millis();
      }
    }
  }
}
