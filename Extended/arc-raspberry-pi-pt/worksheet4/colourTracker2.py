#!/usr/bin/env python3

import camUtil
import cv2
import numpy as np
import pantilthat

p=0
t=0
pantilthat.pan(p)
pantilthat.tilt(t)

lowerLimitRed = np.uint8([0,0,245])
upperLimitRed = np.uint8([255,255,255])

lowerLimitGreen = np.uint8([0,245,0])
upperLimitGreen = np.uint8([255,255,255])

lowerLimit = lowerLimitGreen
upperLimit = upperLimitGreen


camera = camUtil.initCamera()

rawCapture = camUtil.initRawCapture(camera)

# A continuous loop to get frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):

    #Take the current frame and turn it into an image openCV can process
    image = frame.array
    image = cv2.flip(image, 0)

    # Generate a max that only inlcudes pixels from the image in the specified colour ranges
    mask = cv2.inRange(image, lowerLimit, upperLimit)
    
    #Remove some of the smaller individual pixels detected as noise
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    
    #overlay the mask on the original image
    result = cv2.bitwise_and(image, image, mask=mask)   
    
    # find contours in the mask and initialize the current
    # (x, y) center of the ball
    
    cnts = camUtil.grab_contours(mask.copy())
    
    x=0
    y=0

    # only proceed if at least one contour was found
    if len(cnts) > 0:
        # find the largest contour in the mask, then use
        # it to compute the minimum enclosing circle and
        # centroid
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        
        # only proceed if the radius meets a minimum size
        if radius > 10:
            # draw the circle and centroid on the frame
            cv2.circle(result, (int(x), int(y)), int(radius),
                (0, 255, 255), 2)
            cv2.circle(result, (int(x), int(y)), 5, (0, 0, 255), -1) # draw small circle at centre

        if(x<300 and p>-80):
            p= p-1
        elif(x>340 and p<80):
            p=p+1
        if(y<220 and t>-80):
            t = t-1
        elif(y>260 and t<80):
            t = t+1
        pantilthat.pan(p)
        pantilthat.tilt(t)


    cv2.imshow("frame", image)
    cv2.imshow("result", result)

    key = cv2.waitKey(1)
    rawCapture.truncate(0)
    if key == 27:  # Press escape to exit
        break
    elif key == 49: # number 1 key
        lowerLimit = lowerLimitRed
        upperLimit = upperLimitRed
    elif key == 50: # number 2 key
        lowerLimit = lowerLimitGreen
        upperLimit = upperLimitGreen
#     else:
#         print(key)



cv2.destroyAllWindows()
