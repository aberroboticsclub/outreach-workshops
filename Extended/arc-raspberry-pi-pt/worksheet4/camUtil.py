import cv2
import numpy as np 
from picamera.array import PiRGBArray
from picamera import PiCamera

width = 640
height = 480
fps = 25

def nothing(x):
    pass

def initCamera():
    camera = PiCamera()
    camera.resolution = (width, height)
    camera.framerate = fps
    return camera

def initRawCapture(camera):
    rawCapture = PiRGBArray(camera, size=(width, height))
    return rawCapture


#based on imutils convenience.py
#https://github.com/jrosebr1/imutils/blob/master/imutils/convenience.py
def grab_contours(mask):
    
    cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)
    # if the length the contours tuple returned by cv2.findContours
    # is '2' then we are using either OpenCV v2.4, v4-beta, or
    # v4-official
    if len(cnts) == 2:
        cnts = cnts[0]

    # if the length of the contours tuple is '3' then we are using
    # either OpenCV v3, v4-pre, or v4-alpha
    elif len(cnts) == 3:
        cnts = cnts[1]

    # otherwise OpenCV has changed their cv2.findContours return
    # signature yet again and I have no idea WTH is going on
    else:
        raise Exception(("Contours tuple must have length 2 or 3, "
            "otherwise OpenCV changed their cv2.findContours return "
            "signature yet again. Refer to OpenCV's documentation "
            "in that case"))

    # return the actual contours array
    return cnts